import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';

@Component({
  selector: 'app-env-create-status-dialog',
  templateUrl: './env-create-status-dialog.component.html',
  styleUrls: ['./env-create-status-dialog.component.css']
})
export class EnvCreateStatusDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: EnvironmentResponse) {
  }

  ngOnInit(): void {
  }

}
