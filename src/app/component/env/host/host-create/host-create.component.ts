import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormControl, Validators} from '@angular/forms';
import {ImageDataService} from '../../../../service/model/data/image-data.service';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {FlavorDataService} from '../../../../service/model/data/flavor-data.service';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {Host} from '../../../../model/env-config/Host';
import {Flavor} from '../../../../model/env-config/Flavor';

@Component({
  selector: 'app-host-create',
  templateUrl: './host-create.component.html',
  styleUrls: ['./host-create.component.css']
})
export class HostCreateComponent implements OnInit {

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
    Validators.pattern('([a-z_.A-Z_.]*[_.0-9_]*)*')
  ]);
  variableHeight = 4;

  constructor(
    public env: EnvironmentDataService,
    public host: HostDataService,
    public image: ImageDataService,
    public flavor: FlavorDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  goToFlavorCreate() {
    this.router.navigate(['/hosts/flavor/create']);
  }

  goToImageCreate() {
    this.router.navigate(['/images/create']);
  }

  goToImages() {
    this.router.navigate(['/images']);
  }

  updateFlavor(o: Flavor) {
    this.flavor.data = o;
    this.router.navigate(['/hosts/flavor/update']);
  }

  ok() {
    // check that image is selected (flavor is not necessary)
    if (this.host.data.image !== undefined
      && this.nameFormControl.errors == null) {

      this.env.data.hosts.push(this.host.data);

      this.host.data = new Host();
      this.host.data.flavors = new Array<Flavor>();

      this.location.back();
    }
  }

  deleteFlavor(i: number) {
    this.host.data.flavors.splice(i, 1);
  }
}
