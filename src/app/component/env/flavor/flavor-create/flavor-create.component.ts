import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormControl, Validators} from '@angular/forms';
import {Flavor} from '../../../../model/env-config/Flavor';
import {FlavorApiService} from '../../../../service/model/api/flavor-api-service';
import {FlavorDataService} from '../../../../service/model/data/flavor-data.service';
import {HostDataService} from '../../../../service/model/data/host-data.service';

@Component({
  selector: 'app-flavor-create',
  templateUrl: './flavor-create.component.html',
  styleUrls: ['./flavor-create.component.css']
})
export class FlavorCreateComponent implements OnInit {

  flavor = new Flavor();

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
    Validators.maxLength(100),
    Validators.pattern('[a-z_A-Z_]*[_0-9_]*')
  ]);

  pathFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
    Validators.pattern('(/[a-z]*)*')
  ]);

  sizeFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
    Validators.maxLength(8),
    Validators.pattern('[1-9][0-9]*')
  ]);

  constructor(
    private flavorApi: FlavorApiService,
    private flavorCreated: FlavorDataService,
    public host: HostDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  ok() {
    if (this.nameFormControl.errors === null
      && this.sizeFormControl.errors === null
      && this.pathFormControl.errors === null) {
      this.host.data.flavors.push(this.flavor);
      this.goBack();
    }
  }
}
