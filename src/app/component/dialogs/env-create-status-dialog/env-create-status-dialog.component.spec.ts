import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvCreateStatusDialogComponent } from './env-create-status-dialog.component';

describe('EnvCreateStatusDialogComponent', () => {
  let component: EnvCreateStatusDialogComponent;
  let fixture: ComponentFixture<EnvCreateStatusDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvCreateStatusDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvCreateStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
