import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of} from 'rxjs';

import {catchError, finalize} from 'rxjs/operators';

import {EnvironmentApiService} from '../model/api/environment.api.service';
import {Environment} from '../../model/env-config/Environment';

export class EnvDataSource implements DataSource<Environment> {

  private subject = new BehaviorSubject<Environment[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private envService: EnvironmentApiService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<Environment[]> {
    return this.subject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subject.complete();
    this.loadingSubject.complete();
  }

  loadEnvs(pageIndex: number, pageSize: number) {

    this.loadingSubject.next(true);

    this.envService.getEnvConfigs(pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(e => this.subject.next(e));
  }
}
