import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlavorUpdateComponent } from './flavor-update.component';

describe('FlavorUpdateComponent', () => {
  let component: FlavorUpdateComponent;
  let fixture: ComponentFixture<FlavorUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlavorUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlavorUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
