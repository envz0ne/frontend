import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {EnvInstance} from '../../../model/env-inst/EnvInstance';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstanceApiService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private envInstancesUrl = 'http://localhost:8180/environment-provider/environments/instances';

  constructor(private http: HttpClient) {
  }

  // ------------------------------------------Environment's instance----------------------------------------------------

  /** GET env. instances from the server */
  getInstances(pageNumber: number, pageSize: number): Observable<EnvInstance[]> {
    return this.http.get<EnvInstance[]>(this.envInstancesUrl + '/pages',
      {
        params: new HttpParams()
          .set('index', pageNumber.toString())
          .set('size', pageSize.toString())
      }
    )
      .pipe(
        catchError(this.handleError<EnvInstance[]>('getInstances', []))
      );
  }

  getInstance(id: string): Observable<EnvInstance> {
    return this.http.get<EnvInstance>(this.envInstancesUrl + '/' + id, this.httpOptions)
      .pipe(
        catchError(this.handleError<EnvInstance>('getInstance', null))
      );
  }

  // ------------------------------------------Utils----------------------------------------------------

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
