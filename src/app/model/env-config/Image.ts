export class Image {
  public id: string;
  public name: string;
  public path: string;
  public tag: string;


  constructor(id: string, name: string, path: string, tag: string) {
    this.id = id;
    this.name = name;
    this.path = path;
    this.tag = tag;
  }
}
