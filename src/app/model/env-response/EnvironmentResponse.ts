import {EnvironmentStatus} from './EnvironmentStatus';

export class EnvironmentResponse {
  public id: string;
  public info: EnvironmentStatus;
}
