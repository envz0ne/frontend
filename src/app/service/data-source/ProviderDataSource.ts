import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of} from 'rxjs';

import {catchError, finalize} from 'rxjs/operators';
import {ProviderConnection} from '../../model/env-config/ProviderConnection';
import {ProviderApiService} from '../model/api/provider-api-service';

export class ProviderDataSource implements DataSource<ProviderConnection> {

  private subject = new BehaviorSubject<ProviderConnection[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(
    private providerService: ProviderApiService
  ) {
  }

  connect(collectionViewer: CollectionViewer): Observable<ProviderConnection[]> {
    return this.subject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subject.complete();
    this.loadingSubject.complete();
  }

  loadProviders(pageNumber: number, pageSize: number) {

    this.loadingSubject.next(true);

    this.providerService.getProviders(pageNumber, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(e => this.subject.next(e));
  }

  isLoaded() {
    if (this.subject != null
      && typeof this.subject.getValue() !== 'undefined'
      && this.subject.getValue() != null
      && this.subject.getValue().length != null
      && this.subject.getValue().length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
