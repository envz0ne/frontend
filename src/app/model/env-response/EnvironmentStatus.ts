import {EnvStatus} from './EnvStatus';

export class EnvironmentStatus {
  public msg: string;
  public status: EnvStatus;
}
