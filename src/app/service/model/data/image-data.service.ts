import {Injectable} from '@angular/core';
import {Image} from '../../../model/env-config/Image';

@Injectable({
  providedIn: 'root'
})
export class ImageDataService {

  public data: Image;

  constructor() {
  }
}
