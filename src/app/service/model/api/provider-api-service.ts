import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Image} from '../../../model/env-config/Image';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';
import {ProviderConnection} from '../../../model/env-config/ProviderConnection';

@Injectable({
  providedIn: 'root'
})
export class ProviderApiService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private providerUrl = 'http://localhost:8180/environment-provider/providers';

  constructor(private http: HttpClient) {
  }

  // ------------------------------------------Environment's instance----------------------------------------------------

  /** GET env. instances from the server */
  getProviders(pageNumber: number, pageSize: number): Observable<ProviderConnection[]> {
    return this.http.get<ProviderConnection[]>(this.providerUrl + '/pages',
      {
        params: new HttpParams()
          .set('index', pageNumber.toString())
          .set('size', pageSize.toString())
      }
    )
      .pipe(
        catchError(this.handleError<ProviderConnection[]>('getInstances', []))
      );
  }

  getImage(id: string): Observable<ProviderConnection> {
    return this.http.get<ProviderConnection>(this.providerUrl + '/' + id)
      .pipe(
        catchError(this.handleError<ProviderConnection>('getImage', null))
      );
  }

  addProvider(image: any): Observable<ProviderConnection> {
    return this.http.post<ProviderConnection>(this.providerUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<ProviderConnection>('getInstances', null))
      );
  }

  // ------------------------------------------Utils----------------------------------------------------

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
