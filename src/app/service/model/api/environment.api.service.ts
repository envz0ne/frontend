import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Environment} from '../../../model/env-config/Environment';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentApiService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private environmentsUrl = 'http://localhost:8180/environment-provider/environments';

  constructor(private http: HttpClient) {
  }

  // ------------------------------------------Environment's config----------------------------------------------------

  /** GET env. configs from the server */
  getEnvConfigs(pageNumber: number, pageSize: number): Observable<Environment[]> {
    return this.http.get<Environment[]>(this.environmentsUrl + '/pages',
      {
        params: new HttpParams()
          .set('index', pageNumber.toString())
          .set('size', pageSize.toString())
      }
    )
      .pipe(
        catchError(this.handleError<Environment[]>('getEnvConfigs', []))
      );
  }

  /** GET env. config by id. Will 404 if id not found */
  getEnv(id: string): Observable<Environment> {
    const url = `${this.environmentsUrl}/${id}`;
    return this.http.get<Environment>(url).pipe(
      catchError(this.handleError<Environment>(`getEnv id=${id}`))
    );
  }

  /** POST env. */
  add(env: Environment): Observable<HttpResponse<EnvironmentResponse>> {
    return this.http.post<HttpResponse<EnvironmentResponse>>(this.environmentsUrl + '/full-creation', env, this.httpOptions)
      .pipe(
        catchError(this.handleError<HttpResponse<EnvironmentResponse>>(`add env.`))
      );
  }

  /** PUT env. */
  update(env: Environment): Observable<HttpResponse<EnvironmentResponse>> {
    return this.http.post<HttpResponse<EnvironmentResponse>>(this.environmentsUrl + '/' + env.id + '/update', env, this.httpOptions)
      .pipe(
        catchError(this.handleError<HttpResponse<EnvironmentResponse>>(`update env.`))
      );
  }

  /** DELETE env. */
  delete(env: Environment): Observable<HttpResponse<Environment>> {
    return this.http.delete<HttpResponse<Environment>>(this.environmentsUrl + '/' + env.id, this.httpOptions)
      .pipe(
        catchError(this.handleError<HttpResponse<Environment>>(`delete env. by id: ` + env.id))
      );
  }

  runEnv(name: string): Observable<EnvironmentResponse> {
    return this.http.post<EnvironmentResponse>(this.environmentsUrl + '/' + name + '/initiate', this.httpOptions);
  }

  stopEnv(id: string): Observable<EnvironmentResponse> {
    return this.http.post<EnvironmentResponse>(this.environmentsUrl + '/' + id + '/terminate', this.httpOptions);
  }

  // ------------------------------------------Utils----------------------------------------------------

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
