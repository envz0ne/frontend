import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {MaterialModule} from './material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './component/home/home.component';
import {TerminalComponent} from './component/terminal/terminal.component';
import {InstanceComponent} from './component/env/instance/instance/instance.component';
import {EnvConfigComponent} from './component/env/config/env-config/env-config.component';
import {EnvConfigDetailComponent} from './component/env/config/env-config-detail/env-config-detail.component';
import {EnvConfigUpdateComponent} from './component/env/config/env-config-update/env-config-update.component';
import {RouterModule} from '@angular/router';
import {NgTerminalModule} from 'ng-terminal';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {CaruselComponent} from './component/carusel/carusel.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {InstanceDetailComponent} from './component/env/instance/instance-detail/instance-detail.component';
import {EnvConfigRunDialogComponent} from './component/dialogs/env-config-run-dialog/env-config-run-dialog.component';
import { ErrorHandleDialogComponent } from './component/dialogs/error-handle-dialog/error-handle-dialog.component';
import { NoInstanceDialogComponent } from './component/dialogs/no-instance-dialog/no-instance-dialog.component';
import { DocumentationComponent } from './component/documentation/documentation.component';
import { EnvConfigCreateComponent } from './component/env/config/env-config-create/env-config-create.component';
import { HostCreateComponent } from './component/env/host/host-create/host-create.component';
import { FlavorCreateComponent } from './component/env/flavor/flavor-create/flavor-create.component';
import { ImageCreateComponent } from './component/env/image/image-create/image-create.component';
import { ImagesComponent } from './component/env/image/images/images.component';
import { HostUpdateComponent } from './component/env/host/host-update/host-update.component';
import { FlavorUpdateComponent } from './component/env/flavor/flavor-update/flavor-update.component';
import { ProvidersComponent } from './component/env/provider/providers/providers.component';
import { ProviderCreateComponent } from './component/env/provider/provider-create/provider-create.component';
import { ProviderUpdateComponent } from './component/env/provider/provider-update/provider-update.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { EnvCreateStatusDialogComponent } from './component/dialogs/env-create-status-dialog/env-create-status-dialog.component';
import {MaterialElevationDirective} from './MaterialElevationDirective';
import {A11yModule} from '@angular/cdk/a11y';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TerminalComponent,
    InstanceComponent,
    EnvConfigComponent,
    EnvConfigDetailComponent,
    EnvConfigUpdateComponent,
    CaruselComponent,
    InstanceDetailComponent,
    EnvConfigRunDialogComponent,
    ErrorHandleDialogComponent,
    NoInstanceDialogComponent,
    DocumentationComponent,
    EnvConfigCreateComponent,
    HostCreateComponent,
    FlavorCreateComponent,
    ImageCreateComponent,
    ImagesComponent,
    HostUpdateComponent,
    FlavorUpdateComponent,
    ProvidersComponent,
    ProviderCreateComponent,
    ProviderUpdateComponent,
    EnvCreateStatusDialogComponent,
    MaterialElevationDirective
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    NgTerminalModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    NgbModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FlexLayoutModule,
    A11yModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
