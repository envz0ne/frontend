import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EnvConfigDetailComponent} from './env-config-detail.component';

describe('EnvConfigDetailComponent', () => {
  let component: EnvConfigDetailComponent;
  let fixture: ComponentFixture<EnvConfigDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EnvConfigDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvConfigDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
