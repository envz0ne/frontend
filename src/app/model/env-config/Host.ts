import {Image} from './Image';
import {Flavor} from './Flavor';

export class Host {

  public id: string;
  public envId: string;
  public name: string;
  public vcpu: number;
  public ram: number;
  public image: Image;
  public flavors: Array<Flavor>;


  /*constructor(id: string, envId: string, name: string, vcpu: number, ram: number, image: Image, flavors: Array<Flavor>) {
    this.id = id;
    this.envId = envId;
    this.name = name;
    this.vcpu = vcpu;
    this.ram = ram;
    this.image = image;
    this.flavors = flavors;
  }*/
}
