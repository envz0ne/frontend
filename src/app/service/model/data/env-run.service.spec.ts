import { TestBed } from '@angular/core/testing';

import { EnvRunService } from './env-run.service';

describe('EnvRunService', () => {
  let service: EnvRunService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnvRunService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
