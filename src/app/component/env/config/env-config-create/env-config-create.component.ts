import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormControl, Validators} from '@angular/forms';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {EnvironmentApiService} from '../../../../service/model/api/environment.api.service';
import {MatDialog} from '@angular/material/dialog';
import {EnvCreateStatusDialogComponent} from '../../../dialogs/env-create-status-dialog/env-create-status-dialog.component';
import {Host} from '../../../../model/env-config/Host';

@Component({
  selector: 'app-env-config-create',
  templateUrl: './env-config-create.component.html',
  styleUrls: ['./env-config-create.component.css']
})
export class EnvConfigCreateComponent implements OnInit {

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
    Validators.pattern('([a-z_.A-Z_.]*[_.0-9_]*)*')
  ]);
  variableHeight = 4;

  constructor(
    public env: EnvironmentDataService,
    private envApi: EnvironmentApiService,
    public host: HostDataService,
    private createStatusDialog: MatDialog,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  goToHostCreate() {
    this.router.navigate(['/hosts/create']);
  }

  goToProviderCreate() {
    this.router.navigate(['/providers/create']);
  }

  goToProviders() {
    this.router.navigate(['/providers']);
  }

  goToHostUpdate(h: Host) {
    this.host.data = h;
    this.router.navigate(['/hosts/' + h.id + '/update']);
  }

  ok() {
    if (this.nameFormControl.errors == null
      && this.env.data.provider !== undefined
      && this.env.data.hosts.length > 0) {
      this.envApi.add(this.env.data)
        .subscribe(response => {
          this.createStatusDialog.open(EnvCreateStatusDialogComponent,
            {
              data: response
            })
            .afterClosed()
            .subscribe(result => {
              console.log(result);
              if (result) {
                this.router.navigate(['/env-configs']);
              }
            });
        });
    }
  }
}
