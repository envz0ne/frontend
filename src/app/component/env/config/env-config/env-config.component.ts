import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {EnvironmentApiService} from '../../../../service/model/api/environment.api.service';
import {EnvDataSource} from '../../../../service/data-source/EnvDataSource';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {tap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {EnvConfigRunDialogComponent} from '../../../dialogs/env-config-run-dialog/env-config-run-dialog.component';
import {EnvironmentResponse} from '../../../../model/env-response/EnvironmentResponse';
import {InstanceDataService} from '../../../../service/model/data/instance-data.service';
import {InstanceApiService} from '../../../../service/model/api/instance.api.service';
import {Observable, of} from 'rxjs';
import {ErrorHandleDialogComponent} from '../../../dialogs/error-handle-dialog/error-handle-dialog.component';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {Host} from '../../../../model/env-config/Host';
import {Environment} from '../../../../model/env-config/Environment';
import {Flavor} from '../../../../model/env-config/Flavor';
import {EnvInstance} from '../../../../model/env-inst/EnvInstance';

@Component({
  selector: 'app-env-config',
  templateUrl: './env-config.component.html',
  styleUrls: ['./env-config.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvConfigComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'providerName', 'providerType', 'providerUrl', 'connType', 'actions'];

  dataSource: EnvDataSource;
  runData: EnvironmentResponse = null;

  envAmount = 50;
  pageSize = 7;
  pageSizeOptions = [7];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private envApi: EnvironmentApiService,
    private instanceApi: InstanceApiService,
    private instanceData: InstanceDataService,
    public envData: EnvironmentDataService,
    public host: HostDataService,
    private router: Router,
    private route: ActivatedRoute,
    private afterRunDialog: MatDialog,
    private errorDialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataSource = new EnvDataSource(this.envApi);
    this.dataSource.loadEnvs(0, 5);
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadEnvsPage())
      )
      .subscribe();
  }

  loadEnvsPage() {
    this.dataSource.loadEnvs(
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  goToConfig(row: any) {
    this.envData.data = row;
    this.router.navigate(['env-configs/' + row.id]);
  }

  goToCreateEnv() {
    this.envData.data = new Environment();
    this.envData.data.hosts = new Array<Host>();

    this.host.data = new Host();
    this.host.data.flavors = new Array<Flavor>();

    this.router.navigate(['env/create']);
  }

  /* Run the env config */
  runEnv(row: any) {
    this.envApi.runEnv(row.name)
      .subscribe(
        response => {
          this.afterRunDialog.open(EnvConfigRunDialogComponent,
            {
              data: response
            })
            .afterClosed()
            .subscribe(result => {
              // Go to the terminal for the new instance
              if (result) {
                console.log(response);
                this.goToTheInstanceBash(response.id, row.id);
              }
            });
        }
      );
  }

  goToTheInstanceBash(instanceId: string, envId: string) {

    this.instanceData.data = new EnvInstance();

    this.instanceApi.getInstance(instanceId)
      .subscribe(o => {
        console.log(o);
        this.instanceData.data = o;
      });

    if (this.instanceData.data !== undefined) {
      this.router.navigate(['terminal/', envId]);
    } else {
      this.errorDialog.open(ErrorHandleDialogComponent,
        {
          data: 'Can\'t get instance'
        });
    }
  }

  deleteEnv(row: any) {
    this.envApi.delete(row).subscribe(o => this.loadEnvsPage());
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // Run Error Handle Dialog
      this.errorDialog.open(ErrorHandleDialogComponent,
        {
          data: 'After ' + operation + ' something goes wrong.' + ' Error: ' + error
        });

      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
