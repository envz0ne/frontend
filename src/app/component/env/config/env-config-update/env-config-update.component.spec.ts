import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EnvConfigUpdateComponent} from './env-config-update.component';

describe('EnvConfigCrudComponent', () => {
  let component: EnvConfigUpdateComponent;
  let fixture: ComponentFixture<EnvConfigUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EnvConfigUpdateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvConfigUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
