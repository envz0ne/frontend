import {Injectable} from '@angular/core';
import {Environment} from '../../../model/env-config/Environment';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentDataService {

  public data: Environment;

  constructor() {
  }
}
