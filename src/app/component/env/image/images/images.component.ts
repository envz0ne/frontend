import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {EnvironmentResponse} from '../../../../model/env-response/EnvironmentResponse';
import {MatPaginator} from '@angular/material/paginator';
import {tap} from 'rxjs/operators';
import {ImageApiService} from '../../../../service/model/api/image-api-service';
import {ImageDataSource} from '../../../../service/data-source/ImageDataSource';
import {ImageDataService} from '../../../../service/model/data/image-data.service';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {HostDataService} from '../../../../service/model/data/host-data.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'tag', 'actions'];

  dataSource: ImageDataSource;

  envAmount = 50;
  pageSize = 7;
  pageSizeOptions = [7];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private imageApi: ImageApiService,
    public imageSelected: ImageDataService,
    public host: HostDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
    this.dataSource = new ImageDataSource(this.imageApi);
    this.dataSource.loadImages(0, 5);
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadEnvsPage())
      )
      .subscribe();
  }

  loadEnvsPage() {
    this.dataSource.loadImages(
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  goBack() {
    this.location.back();
  }

  select(row: any) {
    this.imageSelected.data = row;
    this.host.data.image = row;
    console.log(this.host.data.image);
    this.goBack();
  }
}
