export class HostInstance {
  public id: string;
  public hostId: string;
  public containerId: string;
  public url: string;
}
