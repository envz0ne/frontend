import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoInstanceDialogComponent } from './no-instance-dialog.component';

describe('NoInstanceDialogComponent', () => {
  let component: NoInstanceDialogComponent;
  let fixture: ComponentFixture<NoInstanceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoInstanceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoInstanceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
