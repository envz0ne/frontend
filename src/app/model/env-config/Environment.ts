import {Host} from './Host';
import {ProviderConnection} from './ProviderConnection';

export class Environment {
  public id: string;
  public name: string;
  public hosts: Host[];
  public provider: ProviderConnection;

/*  constructor(id: string, name: string, hosts: Host[], provider: ProviderConnection) {
    this.id = id;
    this.name = name;
    this.hosts = hosts;
    this.provider = provider;
  }*/
}
