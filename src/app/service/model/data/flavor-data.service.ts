import {Injectable} from '@angular/core';
import {Flavor} from '../../../model/env-config/Flavor';

@Injectable({
  providedIn: 'root'
})
export class FlavorDataService {

  public data: Flavor;

  constructor() {
  }
}
