import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {InstanceDataService} from '../../../../service/model/data/instance-data.service';

@Component({
  selector: 'app-instance-detail',
  templateUrl: './instance-detail.component.html',
  styleUrls: ['./instance-detail.component.css']
})
export class InstanceDetailComponent implements OnInit {

  constructor(
    private router: Router,
    private location: Location,
    public instanceDataService: InstanceDataService) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  stopInstance() {

  }
}

