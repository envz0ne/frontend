import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Image} from '../../../model/env-config/Image';
import {Flavor} from '../../../model/env-config/Flavor';

@Injectable({
  providedIn: 'root'
})
export class FlavorApiService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private imageUrl = 'http://localhost:8180/environment-provider/flavors';

  constructor(private http: HttpClient) {
  }

  // ------------------------------------------Environment's instance----------------------------------------------------

  /** GET env. instances from the server */
  getFlavors(pageNumber: number, pageSize: number): Observable<Flavor[]> {
    return this.http.get<Flavor[]>(this.imageUrl + '/pages',
      {
        params: new HttpParams()
          .set('index', pageNumber.toString())
          .set('size', pageSize.toString())
      }
    )
      .pipe(
        catchError(this.handleError<Flavor[]>('getInstances', []))
      );
  }

  getFlavor(id: string): Observable<Flavor> {
    return this.http.get<Flavor>(this.imageUrl + '/' + id)
      .pipe(
        catchError(this.handleError<Flavor>('getFlavors', null))
      );
  }

  addFlavor(flavor: any): Observable<Flavor> {
    return this.http.post<Flavor>(this.imageUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<Flavor>('getFlavor', null))
      );
  }

  // ------------------------------------------Utils----------------------------------------------------

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
