import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {InstancesDataSource} from '../../../../service/data-source/InstancesDataSource';
import {EnvironmentApiService} from '../../../../service/model/api/environment.api.service';
import {Router} from '@angular/router';

import {tap} from 'rxjs/operators';
import {MatPaginator} from '@angular/material/paginator';
import {InstanceApiService} from '../../../../service/model/api/instance.api.service';
import {InstanceDataService} from '../../../../service/model/data/instance-data.service';

@Component({
  selector: 'app-instance',
  templateUrl: './instance.component.html',
  styleUrls: ['./instance.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'envName', 'createdAt', 'providerLogin', 'providerUrl', 'connectionType', 'actions'];

  dataSource: InstancesDataSource;

  envAmount = 50;
  pageSize = 7;
  pageSizeOptions = [7, 10, 15, 25];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private instanceApiService: InstanceApiService,
    private instanceDataService: InstanceDataService,
    private envService: EnvironmentApiService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.dataSource = new InstancesDataSource(this.instanceApiService, this.envService);
    this.dataSource.loadEnvInstances(0, this.pageSize);
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadEnvsInstancesPage())
      )
      .subscribe();
  }

  loadEnvsInstancesPage() {
    this.dataSource.loadEnvInstances(
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  navToTerminal(row: any) {
    this.instanceDataService.data = row;
    this.router.navigate(['terminal/' + row.id]);
  }

  navigateToDetail(row: any) {
    this.instanceDataService.data = row;
    this.router.navigate(['instances/' + row.id + '/details']);
  }

  stopEnv(row: any) {
    this.dataSource.stopInstance(row);
  }
}
