import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-no-instance-dialog',
  templateUrl: './no-instance-dialog.component.html',
  styleUrls: ['./no-instance-dialog.component.css']
})
export class NoInstanceDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) {
  }

  ngOnInit(): void {
  }

}
