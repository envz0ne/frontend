import {Injectable} from '@angular/core';
import {Host} from '../../../model/env-config/Host';

@Injectable({
  providedIn: 'root'
})
export class HostDataService {

  public data: Host;

  constructor() {
  }
}
