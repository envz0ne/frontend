import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {tap} from 'rxjs/operators';
import {MatPaginator} from '@angular/material/paginator';
import {ProviderApiService} from '../../../../service/model/api/provider-api-service';
import {ProviderDataSource} from '../../../../service/data-source/ProviderDataSource';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.css']
})
export class ProvidersComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'type', 'url', 'connType', 'actions'];

  dataSource: ProviderDataSource;

  envAmount = 50;
  pageSize = 7;
  pageSizeOptions = [7];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private providerApi: ProviderApiService,
    public env: EnvironmentDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
    this.dataSource = new ProviderDataSource(this.providerApi);
    this.dataSource.loadProviders(0, 5);
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadProviders())
      )
      .subscribe();
  }

  loadProviders() {
    this.dataSource.loadProviders(
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  goBack() {
    this.location.back();
  }

  select(row: any) {
    this.env.data.provider = row;
    this.goBack();
  }
}
