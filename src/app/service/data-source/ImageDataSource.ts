import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of} from 'rxjs';

import {catchError, finalize} from 'rxjs/operators';
import {Image} from '../../model/env-config/Image';
import {ImageApiService} from '../model/api/image-api-service';

export class ImageDataSource implements DataSource<Image> {

  private subject = new BehaviorSubject<Image[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private imageService: ImageApiService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<Image[]> {
    return this.subject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subject.complete();
    this.loadingSubject.complete();
  }

  loadImages(pageIndex: number, pageSize: number) {

    this.loadingSubject.next(true);

    this.imageService.getImages(pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(e => this.subject.next(e));
  }
}
