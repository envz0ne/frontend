import { Component, OnInit } from '@angular/core';
import {Flavor} from '../../../../model/env-config/Flavor';
import {FormControl, Validators} from '@angular/forms';
import {FlavorApiService} from '../../../../service/model/api/flavor-api-service';
import {FlavorDataService} from '../../../../service/model/data/flavor-data.service';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-flavor-update',
  templateUrl: './flavor-update.component.html',
  styleUrls: ['./flavor-update.component.css']
})
export class FlavorUpdateComponent implements OnInit {

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
    Validators.maxLength(100),
    Validators.pattern('[a-z_A-Z_]*[_0-9_]*')
  ]);

  pathFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
    Validators.pattern('(/[a-z]*)*')
  ]);

  sizeFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
    Validators.maxLength(8),
    Validators.pattern('[1-9][0-9]*')
  ]);

  constructor(
    private flavorApi: FlavorApiService,
    public flavor: FlavorDataService,
    public host: HostDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  ok() {
    if (this.nameFormControl.errors === null
      && this.sizeFormControl.errors === null
      && this.pathFormControl.errors === null) {
      this.flavor.data = new Flavor();
      this.goBack();
    }
  }
}
