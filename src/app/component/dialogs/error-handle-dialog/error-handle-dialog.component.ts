import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';

@Component({
  selector: 'app-error-handle-dialog',
  templateUrl: './error-handle-dialog.component.html',
  styleUrls: ['./error-handle-dialog.component.css']
})
export class ErrorHandleDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) {
  }

  ngOnInit(): void {
  }

}
