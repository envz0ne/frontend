import {Injectable} from '@angular/core';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';

@Injectable({
  providedIn: 'root'
})
export class EnvRunService {

  public data: EnvironmentResponse;

  constructor() {
  }
}
