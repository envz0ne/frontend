import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {EnvironmentResponse} from '../../../model/env-response/EnvironmentResponse';

@Component({
  selector: 'app-env-config-run-dialog',
  templateUrl: './env-config-run-dialog.component.html',
  styleUrls: ['./env-config-run-dialog.component.css']
})
export class EnvConfigRunDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: EnvironmentResponse) {
  }

  ngOnInit(): void {
  }

}
