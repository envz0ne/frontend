import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {ImageApiService} from '../../../../service/model/api/image-api-service';
import {HostDataService} from '../../../../service/model/data/host-data.service';

@Component({
  selector: 'app-image-create',
  templateUrl: './image-create.component.html',
  styleUrls: ['./image-create.component.css']
})
export class ImageCreateComponent implements OnInit {

  image = new Image();

  constructor(
    public host: HostDataService,
    private imageApi: ImageApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  add() {
    // check image's field
    if (this.image !== undefined) {
      this.imageApi
        .addImage(this.image)
        .subscribe(o => {
          if (this.router.url === '/images/create') {
            this.host.data.image = o;
            console.log('image');
          }
        });
    }
    console.log('image end');
  }
}
