import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './component/home/home.component';
import {EnvConfigComponent} from './component/env/config/env-config/env-config.component';
import {TerminalComponent} from './component/terminal/terminal.component';
import {InstanceComponent} from './component/env/instance/instance/instance.component';
import {EnvConfigDetailComponent} from './component/env/config/env-config-detail/env-config-detail.component';
import {InstanceDetailComponent} from './component/env/instance/instance-detail/instance-detail.component';
import {EnvConfigUpdateComponent} from './component/env/config/env-config-update/env-config-update.component';
import {EnvConfigCreateComponent} from './component/env/config/env-config-create/env-config-create.component';
import {HostCreateComponent} from './component/env/host/host-create/host-create.component';
import {FlavorCreateComponent} from './component/env/flavor/flavor-create/flavor-create.component';
import {ImageCreateComponent} from './component/env/image/image-create/image-create.component';
import {ProviderCreateComponent} from './component/env/provider/provider-create/provider-create.component';
import {ImagesComponent} from './component/env/image/images/images.component';
import {ProvidersComponent} from './component/env/provider/providers/providers.component';
import {HostUpdateComponent} from './component/env/host/host-update/host-update.component';
import {FlavorUpdateComponent} from './component/env/flavor/flavor-update/flavor-update.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'env-configs',
    children: [
      {
        path: '',
        component: EnvConfigComponent
      },
      {
        path: ':id',
        component: EnvConfigDetailComponent
      },
      {
        path: ':id/update',
        component: EnvConfigUpdateComponent
      }
    ]
  },
  {
    path: 'env',
    children: [
      {
        path: 'create',
        component: EnvConfigCreateComponent
      }
    ]
  },
  {
    path: 'hosts',
    children: [
      {
        path: 'create',
        component: HostCreateComponent
      },
      {
        path: ':id/update',
        component: HostUpdateComponent
      },
      {
        path: 'flavor/create',
        component: FlavorCreateComponent
      },
      {
        path: 'flavor/update',
        component: FlavorUpdateComponent
      }
    ]
  },
  {
    path: 'images',
    children: [
      {
        path: 'create',
        component: ImageCreateComponent
      },
      {
        path: '',
        component: ImagesComponent
      }
    ]
  },
  {
    path: 'providers',
    children: [
      {
        path: '',
        component: ProvidersComponent
      },
      {
        path: 'create',
        component: ProviderCreateComponent
      }
    ]
  },
  {
    path: 'instances',
    children: [
      {
        path: '',
        component: InstanceComponent
      },
      {
        path: ':id/details',
        component: InstanceDetailComponent
      }
    ]
  },
  {
    path: 'terminal',
    children: [
      {
        path: '',
        component: TerminalComponent
      },
      {
        path: ':id',
        component: TerminalComponent
      }
    ]
  }

];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule, CommonModule]
})
export class AppRoutingModule {
}
