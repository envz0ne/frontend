import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorHandleDialogComponent } from './error-handle-dialog.component';

describe('ErrorHandleDialogComponent', () => {
  let component: ErrorHandleDialogComponent;
  let fixture: ComponentFixture<ErrorHandleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorHandleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorHandleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
