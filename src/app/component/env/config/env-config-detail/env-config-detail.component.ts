import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Location} from '@angular/common';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {Host} from '../../../../model/env-config/Host';
import {Flavor} from '../../../../model/env-config/Flavor';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {EnvironmentApiService} from '../../../../service/model/api/environment.api.service';

@Component({
  selector: 'app-env-config-detail',
  templateUrl: './env-config-detail.component.html',
  styleUrls: ['./env-config-detail.component.css']
})
export class EnvConfigDetailComponent implements OnInit {

  variableHeight = 4;

  constructor(
    private envApi: EnvironmentApiService,
    public env: EnvironmentDataService,
    public host: HostDataService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  update() {
    this.host.data = new Host();
    this.host.data.flavors = new Array<Flavor>();

    this.router.navigate(['env-configs/' + this.env.data.id + '/update']);
  }

  deleteEnv() {
    this.envApi.delete(this.env.data).subscribe(o => this.router.navigate(['env-configs']));
  }
}
