import { Injectable } from '@angular/core';
import {EnvInstance} from '../../../model/env-inst/EnvInstance';

@Injectable({
  providedIn: 'root'
})
export class InstanceDataService {

  public data: EnvInstance;

  constructor() { }
}
