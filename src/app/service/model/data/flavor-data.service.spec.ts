import { TestBed } from '@angular/core/testing';

import { FlavorDataService } from './flavor-data.service';

describe('FlavorDataService', () => {
  let service: FlavorDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlavorDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
