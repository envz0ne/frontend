import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EnvConfigRunDialogComponent} from './env-config-run-dialog.component';

describe('EnvConfigRunDialogComponent', () => {
  let component: EnvConfigRunDialogComponent;
  let fixture: ComponentFixture<EnvConfigRunDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EnvConfigRunDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvConfigRunDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
