import {HostInstance} from './HostInstance';
import {ProviderConnection} from '../env-config/ProviderConnection';

export class EnvInstance {
  public id: string;
  public envName: string;
  public createdAt: Date;
  public envId: string;
  public provider: ProviderConnection;
  public hosts: Array<HostInstance>;
}
