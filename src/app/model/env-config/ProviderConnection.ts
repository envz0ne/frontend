export class ProviderConnection {

  public id: string;
  public url: string;
  public name: string;
  public type: string;
  public connectionType: string;

  constructor(id: string, name: string, url: string, login: string, type: string, connectionType: string) {
    this.id = id;
    this.url = url;
    this.name = login;
    this.type = type;
    this.connectionType = connectionType;
  }
}
