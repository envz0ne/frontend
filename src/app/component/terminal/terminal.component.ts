import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {DisplayOption, FunctionsUsingCSI, NgTerminal} from 'ng-terminal';

import {Location} from '@angular/common';
import {InstanceDataService} from '../../service/model/data/instance-data.service';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {Terminal} from 'xterm';
import {FormControl} from '@angular/forms';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.css']
})
export class TerminalComponent implements OnInit, AfterViewInit {

  constructor(
    public instance: InstanceDataService,
    private router: Router,
    private location: Location,
    private errorNoInstanceDialog: MatDialog
  ) {
  }

  public resizable: boolean;
  public fixed = true;

  disabled = false;
  rowsControl = new FormControl();
  colsControl = new FormControl();
  inputControl = new FormControl();

  displayOption: DisplayOption = {};
  displayOptionBounded: DisplayOption = {};
  underlying: Terminal;

  @ViewChild('term', {static: true}) child: NgTerminal;

  writeSubject = new Subject<string>();

  inBuffer: string;

  ngOnInit() {
    this.rowsControl.setValue(24);
    this.colsControl.setValue(121);
    this.invalidate();
    this.inBuffer = '';
  }

  ngAfterViewInit() {
    this.underlying = this.child.underlying;
    this.underlying.setOption('fontSize', 16);
    this.underlying.setOption('fontFamily', 'consolas');
    this.underlying.setOption('cursorBlink', true);
    this.underlying.setOption('windowsMode', true);
    this.underlying.setOption('rightClickSelectsWord', true);
    this.underlying.setOption('termName', 'Almost bash');
    this.invalidate();

    this.child.write('Running environment based on the: ' + this.instance.data.envName + ' configuration.');
    this.child.write('\x1b[1E');
    this.child.write('Enter the command to execute it.');
    this.child.write('\x1b[1E');
    this.child.write('\x1b[1E');
    this.child.write('$ ');
    this.child.write('\x1b[1E');
    this.child.write('$ ');
    this.underlying.focus();

    this.child.keyEventInput.subscribe(e => {
      const ev = e.domEvent;

      const printable = !ev.altKey && !ev.ctrlKey && !ev.metaKey;

      if (ev.keyCode === 13) {
        this.child.write('\n' + FunctionsUsingCSI.cursorColumn(1) + '$ '); // \r\n
        this.inBuffer = '';
      } else if (ev.keyCode === 8) {
        // Do not delete the prompt
        if (this.child.underlying.buffer.cursorX > 2) {
          this.child.write('\b \b');
          this.inBuffer = '';
        }
      } else if (printable) {
        this.child.write(e.key);
      }
    });
  }

  invalidate() {
    if (this.resizable) {
      this.displayOption.activateDraggableOnEdge = {minWidth: 100, minHeight: 100};
    } else {
      this.displayOption.activateDraggableOnEdge = undefined;
    }
    if (this.fixed) {
      this.displayOption.fixedGrid = {rows: this.rowsControl.value, cols: this.colsControl.value};
    } else {
      this.displayOption.fixedGrid = undefined;
    }
    this.child.setDisplayOption(this.displayOption);
  }

  resizableChange(event: MatSlideToggleChange) {
    this.resizable = event.checked;
    if (this.resizable) {
      this.fixed = false;
    }
    this.invalidate();
  }

  fixedChange(event: MatSlideToggleChange) {
    this.fixed = event.checked;
    if (this.fixed) {
      this.resizable = false;
    }
    this.invalidate();
  }

  write() {
    // tslint:disable-next-line:no-eval
    this.writeSubject.next(eval(`'${this.inputControl.value}'`));
  }

  // Key stroke down
  onKeyInput(event: string) {

    if (this.isLegitSymbol(event)) {
      this.inBuffer += event;
      console.log(this.inBuffer);
    }

    // Enter cmd was pushed
    if (event === '\x0D') {

    }


  }

  goBack() {
    this.location.back();
  }

  isLegitSymbol(str) {
    // tslint:disable-next-line:one-variable-per-declaration
    let code, i, len;

    for (i = 0, len = str.length; i < len; i++) {
      code = str.charCodeAt(i);
      if (
        !(code > 46 && code < 58) && // numeric (&, 0-9)
        !(code > 64 && code < 91) && // upper alpha (A-Z)
        !(code > 96 && code < 123) && // lower alpha (a-z)
        code !== 45 && code !== 36 && code !== 38 && // =, $, &
        code !== 40 && code !== 41 && code !== 42 && // (, ), *
        code !== 45 && code !== 46 && code !== 47 && // -, ., /
        code !== 58 && code !== 61 && code !== 59 && code !== 32 && // :, =, ;, SPC
        code !== 124 && code !== 126 && !(code > 210 && code < 213) && // |, ~, ",'
        code !== 91 && code !== 93 && code !== 123 && code !== 125 // [, ], {, }
        && code !== 95 && code !== 63 && code !== 94 && code !== 64) { // _, ?, ^, @
        return false;
      }
    }
    return true;
  }
}
