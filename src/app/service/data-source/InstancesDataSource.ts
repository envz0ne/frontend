import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of} from 'rxjs';

import {catchError, finalize} from 'rxjs/operators';
import {EnvInstance} from '../../model/env-inst/EnvInstance';
import {InstanceApiService} from '../model/api/instance.api.service';
import {EnvironmentApiService} from '../model/api/environment.api.service';

export class InstancesDataSource implements DataSource<EnvInstance> {

  private subject = new BehaviorSubject<EnvInstance[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private instanceService: InstanceApiService, private envService: EnvironmentApiService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<EnvInstance[]> {
    return this.subject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subject.complete();
    this.loadingSubject.complete();
  }

  loadEnvInstances(pageNumber: number, pageSize: number) {

    this.loadingSubject.next(true);

    this.instanceService.getInstances(pageNumber, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(e => this.subject.next(e));
  }

  stopInstance(row) {
    this.envService.stopEnv(row.id)
      .subscribe(() => this.loadEnvInstances(0, 7));
  }

  isLoaded() {
    if (this.subject != null
      && typeof this.subject.getValue() !== 'undefined'
      && this.subject.getValue() != null
      && this.subject.getValue().length != null
      && this.subject.getValue().length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
