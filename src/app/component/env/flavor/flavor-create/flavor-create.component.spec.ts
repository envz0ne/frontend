import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlavorCreateComponent } from './flavor-create.component';

describe('FlavorCreateComponent', () => {
  let component: FlavorCreateComponent;
  let fixture: ComponentFixture<FlavorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlavorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlavorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
