import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {EnvironmentDataService} from '../../../../service/model/data/environment-data.service';
import {FormControl, Validators} from '@angular/forms';
import {Host} from '../../../../model/env-config/Host';
import {HostDataService} from '../../../../service/model/data/host-data.service';
import {EnvironmentApiService} from '../../../../service/model/api/environment.api.service';
import {EnvCreateStatusDialogComponent} from '../../../dialogs/env-create-status-dialog/env-create-status-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-env-config-crud',
  templateUrl: './env-config-update.component.html',
  styleUrls: ['./env-config-update.component.css']
})
export class EnvConfigUpdateComponent implements OnInit {

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
    Validators.pattern('([a-z_.A-Z_.]*[_.0-9_]*)*')
  ]);
  variableHeight = 4;

  constructor(
    public env: EnvironmentDataService,
    public host: HostDataService,
    private envApi: EnvironmentApiService,
    private updateStatusDialog: MatDialog,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  goToProviders() {
    this.router.navigate(['/providers']);
  }

  goToProviderCreate() {
    this.router.navigate(['/providers/create']);
  }


  goToHostCreate() {
    this.router.navigate(['/hosts/create']);
  }

  goToHostUpdate(h: Host) {
    this.host.data = h;
    this.router.navigate(['/hosts/' + h.id + '/update']);
  }

  ok() {
    console.log(this.env.data);
    this.envApi.update(this.env.data)
      .subscribe(response => {
        this.updateStatusDialog.open(EnvCreateStatusDialogComponent,
          {
            data: response
          })
          .afterClosed()
          .subscribe(result => {
            console.log(result);
            if (result) {
              this.router.navigate(['/env-configs']);
            }
          });
      });
  }
}
