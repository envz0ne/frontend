import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvConfigCreateComponent } from './env-config-create.component';

describe('EnvConfigCreateComponent', () => {
  let component: EnvConfigCreateComponent;
  let fixture: ComponentFixture<EnvConfigCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvConfigCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvConfigCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
